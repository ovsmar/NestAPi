import {ApiProperty} from "@nestjs/swagger";
import {IsEmail} from "class-validator";

export class SigninDto {
    @ApiProperty({
        example: 'test@gmail.com',
        description: 'edentifiant du compte'
    })
    @IsEmail()
    email: string;

    @ApiProperty({
        type: String,
        example: 'password'
    })
    password: string;

}