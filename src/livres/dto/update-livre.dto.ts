import {PartialType} from '@nestjs/swagger';
import {CreateLivreDto} from './create-livre.dto';
import {ApiProperty} from '@nestjs/swagger';
import {IsOptional, IsString, IsDate} from 'class-validator';

export class UpdateLivreDto extends PartialType(CreateLivreDto) {
    @ApiProperty({
        description: 'Title of the book',
        required: false
    })
    @IsOptional()
    @IsString()
    title?: string;

    @ApiProperty({
        description: 'Author of the book',
        required: false
    })
    @IsOptional()
    @IsString()
    author?: string;

    @ApiProperty({
        description: 'Published date of the book',
        required: false
    })
    @IsOptional()
    publishedDate: string;
}