import {ApiProperty} from "@nestjs/swagger";
import {IsNotEmpty} from "class-validator";

export class CreateLivreDto {
    @ApiProperty({
        description: 'Title of the book',
        required: true
    })
    @IsNotEmpty()
    title: string;

    @ApiProperty({
        description: 'Author of the book',
        required: true
    })
    @IsNotEmpty()
    author: string;

    @ApiProperty({
        description: 'Published date of the book',
        required: true
    })
    @IsNotEmpty()
    publishedDate: string;
}