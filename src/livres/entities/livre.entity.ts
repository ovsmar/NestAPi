import {ApiProperty} from '@nestjs/swagger';
import {Column, Entity, ObjectIdColumn} from 'typeorm';

@Entity('livres')
export class Livre {
    @ObjectIdColumn()
    @ApiProperty({type: Number, description: 'ID', example: 1})
    id: string;

    @ApiProperty({
        type: String,
        description: 'Title of the book',
        example: 'Book Title',
    })
    @Column({type: String, nullable: false})
    title: string;

    @ApiProperty({
        type: String,
        description: 'Author of the book',
        example: 'Author Name',
    })
    @Column({type: String, nullable: false})
    author: string;

    @ApiProperty({
        type: Date,
        description: 'Published date of the book',
        example: '2023-12-13',
    })
    @Column({type: 'date', nullable: false})
    publishedDate: string;
}