import {HttpException, HttpStatus, Injectable, NotFoundException} from '@nestjs/common';
import {CreateLivreDto} from './dto/create-livre.dto';
import {UpdateLivreDto} from './dto/update-livre.dto';
import {Livre} from './entities/livre.entity';
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";


@Injectable()
export class LivresService {
    constructor(
        @InjectRepository(Livre)
        private readonly livreRepository: Repository<Livre>,
    ) {
    }

    async create(createLivreDto: CreateLivreDto) {
        if (!createLivreDto) {
            throw new HttpException('createLivreDto must be provided', HttpStatus.BAD_REQUEST);
        }

        let newLivre;
        try {
            newLivre = this.livreRepository.create(createLivreDto);
            await this.livreRepository.save(newLivre);
        } catch (error) {
            throw new HttpException('An error occurred while saving the Livre', HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return newLivre;
    }


    findAll() {
        return this.livreRepository.find();
    }

    async findOne(title: string) {
        const livre = await this.livreRepository.findOneBy({'title': title});
        if (!livre) {
            throw new NotFoundException(`Livre with title "${title}" not found`);
        }
        return livre;
    }


    async update(idLivre: string, updateLivreDto: UpdateLivreDto) {
        const updateResult = await this.livreRepository.update(idLivre, updateLivreDto);
        if (updateResult.affected === 0) {
            throw new HttpException(`Livre with ID "${idLivre}" not found`, HttpStatus.NOT_FOUND);
        }
        return this.livreRepository.findOneBy({"id": idLivre});
    }


    async remove(id: string) {
        const deleteResult = await this.livreRepository.delete(id);
        if (deleteResult.affected === 0) {
            throw new NotFoundException(`Livre with ID "${id}" not found`);
        }
        return `Livre with ID "${id}" has been removed`;
    }
}
