import {Controller, Get, Post, Body, Patch, Param, Delete, UseGuards} from '@nestjs/common';
import {LivresService} from './livres.service';
import {CreateLivreDto} from './dto/create-livre.dto';
import {UpdateLivreDto} from './dto/update-livre.dto';
import {ApiBearerAuth, ApiTags} from "@nestjs/swagger";
import {AuthGuard} from "../auth/auth.guard";

@ApiTags('Livres')
@UseGuards(AuthGuard)
@ApiBearerAuth()
@Controller('livres')
export class LivresController {
    constructor(private readonly livresService: LivresService) {
    }

    @Post()
    create(@Body() createLivreDto: CreateLivreDto) {
        return this.livresService.create(createLivreDto);
    }

    @Get()
    findAll() {
        return this.livresService.findAll();
    }

    @Get(':title')
    findOne(@Param('title') title: string) {
        return this.livresService.findOne(title);
    }

    @Patch(':id')
    update(@Param('id') id: string, @Body() updateLivreDto: UpdateLivreDto) {
        return this.livresService.update(id, updateLivreDto);
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.livresService.remove(id);
    }
}
