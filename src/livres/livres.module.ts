import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Livre } from './entities/livre.entity';
import { LivresService } from './livres.service';
import {LivresController} from "./livres.controller";

@Module({
  imports: [TypeOrmModule.forFeature([Livre])],
  controllers: [LivresController],
  providers: [LivresService],
  exports: [LivresService],
})
export class LivresModule {}