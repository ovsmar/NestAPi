import {Injectable} from '@nestjs/common';
import {CreateUserDto} from './dto/create-user.dto';
import {UpdateUserDto} from './dto/update-user.dto';
import {InjectRepository} from "@nestjs/typeorm";
import {User} from "./entities/user.entity";
import {Repository} from "typeorm";
import * as bcrypt from 'bcrypt';
import {HttpException, HttpStatus} from '@nestjs/common';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User)
        private usersRepository: Repository<User>,
    ) {
    }

    async create(createUserDto: CreateUserDto) {
        const hashedPassword = await bcrypt.hash(createUserDto.password, 10);
        createUserDto.password = hashedPassword;
        return this.usersRepository.save(createUserDto);
    }

    findAll() {
        return this.usersRepository.find();
    }

    findOne(email: string) {
        return this.usersRepository.findOneBy({'email': email});
    }


    async update(idUser: string, updateUserDto: UpdateUserDto) {
        try {
            if (updateUserDto.password) {
                const hashedPassword = await bcrypt.hash(updateUserDto.password, 10);
                updateUserDto.password = hashedPassword;
            }
            const result = await this.usersRepository.update({id: idUser}, updateUserDto);
            if (!result.affected) {
                throw new HttpException('User not found', HttpStatus.NOT_FOUND);
            }
            return result;
        } catch (error) {
            throw new HttpException({
                status: HttpStatus.INTERNAL_SERVER_ERROR,
                error: 'There was a problem updating the user',
            }, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    async remove(idUser: string) {
        try {
            const result = await this.usersRepository.delete({id: idUser});
            if (!result.affected) {
                throw new HttpException('User not found', HttpStatus.NOT_FOUND);
            }
            return result;
        } catch (error) {
            throw new HttpException({
                status: HttpStatus.INTERNAL_SERVER_ERROR,
                error: 'There was a problem deleting the user',
            }, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}