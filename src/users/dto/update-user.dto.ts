import {PartialType} from '@nestjs/mapped-types';
import {CreateUserDto} from './create-user.dto';
import {ApiProperty} from "@nestjs/swagger";

export class UpdateUserDto extends PartialType(CreateUserDto) {

    @ApiProperty({
        description: 'Prénom'
    })
    firstName: string;

    @ApiProperty({
        description: 'Nom'
    })
    lastName: string;

    @ApiProperty({
        description: 'Mail'
    })
    email: string;

    @ApiProperty({
        description: 'Mot de passe'
    })
    password: string;
}
