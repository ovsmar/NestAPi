import {ApiProperty} from "@nestjs/swagger";
import {IsEmail, IsNotEmpty, MinLength} from "class-validator";

export class CreateUserDto {
    @ApiProperty({
        description: 'Prénom',
        required: true
    })
    @IsNotEmpty()
    firstName: string;

    @ApiProperty({
        description: 'Nom',
        required: true
    })
    lastName: string;

    @ApiProperty({
        description: 'Mail',
        required: true
    })
    @IsEmail()
    @IsNotEmpty()
    email: string;

    @ApiProperty({
        description: 'Mot de passe',
        required: true
    })
    @IsNotEmpty()
    @MinLength(8)
    password: string;
}