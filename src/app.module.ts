import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {UsersModule} from './users/users.module';
import {TypeOrmModule, TypeOrmModuleAsyncOptions, TypeOrmModuleOptions} from '@nestjs/typeorm';
import {AuthModule} from './auth/auth.module';
import {ConfigModule, ConfigService} from '@nestjs/config';
import { LivresModule } from './livres/livres.module';

@Module({
    imports: [
        ConfigModule.forRoot(),
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: async (configService: ConfigService) => ({
                type: configService.get<string>('DATABASE_TYPE') as any,
                url: configService.get<string>('DATABASE_URL'),
                database: configService.get<string>('DATABASE_NAME'),
                entities: [__dirname + '/**/*.entity{.ts,.js}'],
                ssl: true,
                // useUnifiedTopology: true,
                // useNewUrlParser: true,
                synchronize: true, //not for production
            }),
        }),
        UsersModule,
        AuthModule,
        LivresModule,
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}



